# dotfiles

Get the dotfiles:

    git clone git@gitlab.com:forst6/dotfiles.git ~/dotfiles
    cd dotfiles
    ./install.sh link

# Installations

[Install homebrew](https://docs.brew.sh/Installation)

    git clone https://github.com/Homebrew/brew ~/homebrew
    eval "$(homebrew/bin/brew shellenv)"
    brew update --force --quiet
    chmod -R go-w "$(brew --prefix)/share/zsh"

Now install the stuff you want. See [Brewfiles/README.md](Brewfiles/README.md)

## Install Atom Packages

    apm install --packages-file atom_packages.txt

To get your currently installed Atom packages run `apm list --bare --installed --versions=false`.

## Docker

1. Install from https://docs.docker.com/desktop/mac/install/
1. To pull our Images from non-public Gitlab.com registries:  
   Get a **GitLab Personal Access Token** with Scope: `read_registry` at https://gitlab.com/-/profile/personal_access_tokens  
   and use it at the following command:
1. `docker login registry.gitlab.com -u forst6`
1. now try `docker pull registry.gitlab.com/guj-digital/das/ansible-in-a-container:latest`

## [aws-rotate-iam-keys](https://aws-rotate-iam-keys.com/)

Since i prefer installing Homebrew to `$HOME/homebrew`, some steps have to be
made to get automatic aws access keys rotation with `aws-rotate-iam-keys`.

**No privileged access necessary!**

1. Be sure `aws-rotate-iam-keys` is installed and `~/.aws-rotate-iam-keys` is at it's place.
1. Edit file `"$(brew --prefix aws-rotate-iam-keys)/homebrew.mxcl.aws-rotate-iam-keys.plist"`
   and add the `homebrew/bin/` path:
   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
   <plist version="1.0">
   <dict>
     <key>EnvironmentVariables</key>
       <dict>
         <key>PATH</key>
         <string>/Users/forst.sebastian/homebrew/bin/:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin</string>
       </dict>
   ```
1. Also change the **time**, when you want to rotate the keys.
1. Now run `brew services restart aws-rotate-iam-keys`.

Check `/tmp/homebrew.mxcl.aws-rotate-iam-keys.log` for success.


See:
- https://github.com/rhyeal/aws-rotate-iam-keys#macos-1
- https://github.com/rhyeal/aws-rotate-iam-keys/issues/67
