# Installs software with homebrew

## Install basic cli tools

    brew bundle install --file=basic.Brewfile

## Install GUI Apps

    brew bundle install --file=GUI-Apps.Brewfile
